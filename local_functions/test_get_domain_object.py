import os
import pickle

from xpms_helper.model.document_util import get_document_object
from xpms_helper.model.domain_util import get_domain_object
from xpms_helper.model.recommendation_util import get_recommendation_object
from xpms_helper.model.model_utils import load, save
from xpms_helper.executions.execution_variables import ExecutionVariables

file_path = '/tmp'


# def test_get_document_object(doc_id):
#     filter_obj = {'solution_id':'demotest','doc_id':doc_id, 'root_id':doc_id}
#     doc_obj = get_document_object(filter_obj)
#     print(doc_obj)


def test_get_domain_object(doc_id):
    filter_obj = {'solution_id':'demotest','doc_id':doc_id, 'root_id':doc_id}
    doc_obj = get_domain_object(filter_obj)
    print(doc_obj)

# def test_get_recommendation_object(doc_id):
#     filter_obj = {'solution_id':'demotest','doc_id':doc_id, 'root_id':doc_id}
#     doc_obj = get_recommendation_object(filter_obj)
#     print(doc_obj)
#
#
# def test_save_pkl(file_name):
#     config = {'src_dir':file_path}
#     pkl_obj = "this is a pickle file"
#     save(file_name, pkl_obj, config)
#
#
# def test_load(file_name):
#     full_file_path = "{0}/{1}".format(file_path, file_name)
#     config = {'src_dir': full_file_path}
#     pk_file = load(file_name, config)
#     pkl_obj = "this is a pickle file"
#     assert pk_file == pkl_obj
#
#
# def test_set_execution_variables():
#     context = {}
#     exec_ins = ExecutionVariables.get_instance(context)
#     exec_ins.set_variable('df', 'pravin')
#
#
# def test_get_execution_variables():
#     context = {}
#     exec_ins = ExecutionVariables.get_instance(context)
#     exec_ins.get_variable('df')
